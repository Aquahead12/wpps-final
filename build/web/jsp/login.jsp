<%-- 
    Document   : index
    Created on : 08 14, 18, 2:24:25 PM
    Author     : Tim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>WPPS Portal - Login</title>

        <!-- Bootstrap core CSS-->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

        <!-- Custom styles for this template-->
        <link href="css/sb-admin.css" rel="stylesheet">

    </head>

    <body background="images/mainbg3.jpg" style="background-repeat: no-repeat; background-size: cover;">

        <div class="container">
            <div class="card card-login mx-auto mt-5">
                <div class="card-header">Please Sign In</div>
                <div class="card-body">
                    <center><img src="images/rcbc-logo.png" height="200" width="200" /></center>
                    <div class="form-group">
                        <h5>
                            <center>Log In to Web Portal</center>
                        </h5>
                    </div>
                    <form action="LoginServlet" method="post">
                        <div class="form-group">
                            <div class="form-label-group">
                                <input type="email" id="email" name="email" class="form-control" placeholder="Email address" required="required" autofocus="autofocus">
                                <label for="email">Email address</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-label-group">
                                <input type="password" id="password" name="password" class="form-control" placeholder="Password" required="required">
                                <label for="password">Password</label>
                            </div>
                        </div>
                        <button class="btn btn-primary btn-block" id="btnsubmit" type="submit">Login</button>
                    </form>
                </div>
                <div class="card-footer">
                    <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright © WPPS Portal 2018</span>
                    </div>
                </div>
                    
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    </body>

</html>
